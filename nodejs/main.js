var app = angular.module('myApp', []);

app.controller('myCtrl', function ($scope, $http) {


  $scope.getMethod = function () {
    var id = $scope.id;
    console.log("Id value " + id);
    console.log("Get Method!!!");

    $http({
      method: 'GET',
      url: 'http://localhost:3000/'+id
    }).then(function successCallback(response) {
    	console.log("success");
      console.log(response.data);
    }, function errorCallback(response) {
    	console.log("Failure");
    });

    

  }

});
