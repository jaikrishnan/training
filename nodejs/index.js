'use strict';

const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const schema = require('schema/schema.js')
const server = new Hapi.Server();
const DBconnection = require('./db_init');
var r = require('rethinkdb');

var jsonString = "";
var post = "";
var joi = require('joi');
var fs = require("fs");
var data;


var connection = new DBconnection();
connection.initConnection();


server.connection({
    host: (process.env.HOST || 'localhost'),
    port: (process.env.PORT || 3000),
    routes: { cors: true }
});
const options = {
    info: {
        'title': 'Test API Documentation',
        'version': Pack.version,
    }
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Server running at:');
            }
        });
    });



server.route({
    method: 'GET',
    path: '/{id}',
    handler: function (request, reply) {
        connection.initConnection().then((res) => {
                connection.getById(request.params.id).then((res1) => {
                    console.log("Console log "+JSON.stringify(res1));
                })    
        })

        var result = [];
        data = fs.readFileSync('json/' + request.params.id + '.json');
        jsonString = JSON.parse(data);
        //console.log(data);
        for (var i = 0; i < jsonString.length; i++) {
            result = result + jsonString[i].code;
        }
        //console.log(result);
        reply(result);
    },
    config: {
        tags: ['api'],
        validate: {
            params: {
                id: joi.string().valid(['user1', 'user2', 'user3']).required()
            }
        }
    }
});

server.route({
    method: 'POST',
    path: '/',
    handler: function (request, reply) {

        var responseBody = {
            id: request.payload.id,
            language: request.payload.language,
            code: request.payload.code,
            createdDate: Date(),
            modifiedDate: Date()
        }

        var db = new DBconnection();
        db.initConnection().then((res) => {
            connection.insertById(request.payload.id,
            request.payload.language,request.payload.code).then((res1) => {
                    console.log("Inserted Successfully");
            }, (err) => { reply(err)});


        }, (err) => {

        })


        data = fs.readFileSync('json/' + request.payload.id + '.json');
        jsonString = JSON.parse(data);
        jsonString.push(JSON.parse(JSON.stringify(responseBody)));
        fs.writeFileSync('json/' + request.payload.id + '.json', JSON.stringify(jsonString));
        reply(responseBody);
    },
    config: {
        tags: ['api'],
        validate: {
            payload: schema.post
        }
    }
});

server.route({
    method: 'PUT',
    path: '/',
    handler: function (request, reply) {

    connection.initConnection().then((res) => {
        connection.update(request.payload.id,request.payload.code).then((res) => {
            console.log("Updated Successfully");
                },(err)=>{
                    reply('Update Validation Error');
                })
    })

        data = fs.readFileSync('json/' + request.payload.id + '.json');
        jsonString = JSON.parse(data);

        for (var i = 0; i < jsonString.length; i++) {
            if (jsonString[i].id == request.payload.id &&
                jsonString[i].language == request.payload.language) {
                console.log("Value of " + i);
                jsonString[i].code = request.payload.code;
                jsonString[i].modifiedDate = Date();
            }
        }

        //jsonString.push(JSON.parse(JSON.stringify(responseBody)));
        fs.writeFileSync('json/' + request.payload.id + '.json', JSON.stringify(jsonString));
        reply("Put method End");
    },
    config: {
        tags: ['api'],
        validate: {
            payload: schema.post
        }
    }
});

server.route({
    method: 'DELETE',
    path: '/',
    handler: function (request, reply) {

        connection.initConnection().then((res) => {
        connection.deleteFunction(request.payload.id).then((res) => {
            console.log("Deleted Successfully");
                })
    })
        

        data = fs.readFileSync('json/' + request.payload.id + '.json');
        jsonString = JSON.parse(data);
        for (var i = 0; i < jsonString.length; i++) {
            if (jsonString[i].id == request.payload.id &&
                jsonString[i].language == request.payload.language) {
                console.log(i);
                jsonString.slice(i, 1);
            }
        }

        //jsonString.push(JSON.parse(JSON.stringify(responseBody)));
        fs.writeFileSync('json/' + request.payload.id + '.json', JSON.stringify(jsonString));
        reply("Deleted Successfully !!!");
    },
    config: {
        tags: ['api'],
        validate: {
            payload: schema.del
        }
    }
});


function getDate() {
    var date = new Date();
    return date;
}

function returnSnippet() {

}