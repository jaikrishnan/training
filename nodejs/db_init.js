'use strict';
var r = require('rethinkdb');
var joi = require('joi');
var validate = require('schema/db_schema.js')
var co = require('co');

var _conn;
class DBConnection {
    constructor() {
        this.conn = _conn;
    }
    initConnection() {
        let me = this;
        return co(function* () {
            try {
                if (!_conn) {
                    _conn = yield r.connect({ host: 'localhost', port: 28015 });
                    me.conn = _conn;
                }
            } catch (err) {
                console.log(err);
            }
            //console.log(_conn);
        })
    }

    insertById(post_id, post_language, post_code) {
        let me = this;
        return co(function* () {
            try {
                joi.validate({id : post_id,language:post_language,
                    post_code:code},validate.dbpost,(err,res)=>{
                    if(err)
                        throw err;
                })
                r.table("posts").insert({
                    "id": post_id,
                    "title": post_language,
                    "content": post_code
                }).run(me.conn)
            } catch (err) {
                console.log(err);
            }
            //console.log(_conn);
            console.log("Executing");
            me.conn = _conn;
        })

    }

    getById(get_id) {

        let me = this;
        return co(function* () {
            try {
                
                var x = yield r.table('posts').filter({'id':get_id}).run(me.conn)
                var result = yield x.toArray();
                console.log("Result "+JSON.stringify(result));
                return result;
            } catch (err) {
                console.log(err);
            }
            //console.log(_conn);
            console.log("Executing");
            me.conn = _conn;
        })

    }

    update(get_id,code){
          let me = this;
        return co(function* () {
            try {
                joi.validate({id : post_id,
                    post_code:code},validate.del,(err,res)=>{
                    if(err)
                        throw err;
                })
                var x = yield r.table('posts').filter({'id':get_id}).update({"code":code}).run(me.conn)
                var result = yield x.toArray();
                console.log("Result "+JSON.stringify(result));
                return result;
            } catch (err) {
                console.log(err);
            }
            //console.log(_conn);
            console.log("Executing");
            me.conn = _conn;
        })
    }

    deleteFunction(get_id){
        let me = this;
        return co(function* () {
            joi.validate({id : post_id,
                    post_code:code},validate.del,(err,res)=>{
                    if(err)
                        throw err;
                })
            try { 
                var x = yield r.table('posts').filter({'id':get_id}).delete().run(me.conn)
                return result;
            } catch (err) {
                console.log(err);
            }
            me.conn = _conn;
        })
    }

}

module.exports = DBConnection;
